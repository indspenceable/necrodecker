﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Encounters/Standard")]
public class EncounterData : ReferrableScriptableObject
{
    public List<MookPlannerBase> Monsters;
}