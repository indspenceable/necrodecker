﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public interface ICardDragHandler
{
    bool ExecuteDrag(CardPoolable cardPoolable, Vector3 ped);
}

public class CardPoolable : AbstractPoolable, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Image image;
    public TMPro.TextMeshProUGUI textDisplay;
    public TMPro.TextMeshProUGUI PowerCost;
    public TMPro.TextMeshProUGUI RemainingUses;
    public TMPro.TextMeshProUGUI cardTitle;
    public CardInstance inst;

    public ICardDragHandler dragHandler;

    [Space]
    public DragPool DragIndicatorPool;
    private DragIndicator di;

    public override void ExternalDeactivate()
    {
        gameObject.SetActive(false);
    }

    internal void Setup(ICardDragHandler dragHandler, CardInstance instance)
    {
        this.dragHandler = dragHandler;
        this.inst = instance;
        image.sprite = instance.def.picture;
        this.textDisplay.text = instance.def.text;
        this.cardTitle.text = instance.def.name;

        RemainingUses.text = $"{instance.RemainingCharges}/{instance.def.DefaultCharges}";
    }

    public override void OnActivatedByPool()
    {
        gameObject.SetActive(true);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        var rtn = dragHandler.ExecuteDrag(this, eventData.pointerCurrentRaycast.worldPosition);
        di.ReleaseDrag(eventData.pointerCurrentRaycast.worldPosition, rtn);
        di = null;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        this.di = DragIndicatorPool.RequestWithOrigin(eventData.pointerPressRaycast.worldPosition);
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        this.di.SetDest(eventData.pointerCurrentRaycast.worldPosition);
    }
}
