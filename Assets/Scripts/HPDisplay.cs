﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPDisplay : MonoBehaviour
{
    public TMPro.TextMeshProUGUI hpDisplay;
    public MookBase mb;

    // Update is called once per frame
    void Update()
    {
        hpDisplay.text = $"{mb.HP} hp";
    }
}
