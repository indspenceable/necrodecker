﻿using System.Collections;
using UnityEngine;
//[CreateAssetMenu(menuName = "Status Effect/Poison")]
//public class PoisonStatusEffect : StatusDefinition { }
[CreateAssetMenu(menuName = "Status Effect/Poison")]
public class PoisonStatusEffect : StatusDefinition
{

    public override IEnumerator StartOfTurn(MookBase m, int stacks)
    {
        yield return null;
        ApplyPoison(m, stacks);
    }

    private void ApplyPoison(MookBase m, int stacks)
    {
        m.TakeAttack(null, stacks);
        m.AddStatus(this, -1);
    }

    public override IEnumerator EndOfTurn(MookBase m, int stacks) {
        yield return null;
    }
}
