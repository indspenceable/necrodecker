﻿[System.Serializable]
public class CardInstance
{
    public int RemainingCharges = 0;
    public CardDefinition def;

    public CardInstance(CardDefinition def)
    {
        this.def = def;
        this.RemainingCharges = def.DefaultCharges;
    }
}