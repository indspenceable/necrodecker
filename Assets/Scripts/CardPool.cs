﻿using UnityEngine;

[CreateAssetMenu(menuName = "Object Pools/Cards")]
public class CardPool : ObjectPoolBase<CardPoolable>
{
    public CardPoolable RequestCard(ICardDragHandler dragHandler, CardInstance instance, Transform t)
    {
        CardPoolable card = Request(Vector3.zero);
        card.transform.SetParent(t);
        card.transform.localScale = Vector3.one;
        card.transform.position = Vector3.zero;
        card.Setup(dragHandler, instance);
        return card;
    }
}