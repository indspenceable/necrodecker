﻿using UnityEngine;

public abstract class MookPlannerBase : ReferrableScriptableObject
{
    public Sprite appearance;
    public int MaxHP;
    public abstract Mook.IMookActionPlan DeterminePlan();
}
