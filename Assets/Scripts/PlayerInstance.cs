﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInstance : MookBase
{
    public List<CardInstance> Deck;
    public List<CardInstance> CurrentHand;
    public List<CardInstance> Graveyard;
    public int Energy;
    public PlayerHandUI phui;
    public CardBattle battle;


    internal void GainBlock(int block)
    {
        CurrentBlock += block;
    }

    public void DrawCards(int count)
    {
        for (int i = 0; i < count; i +=1)
        {
            if (Deck.Count == 0)
            {
                Deck = Graveyard.Shuffled();
                Graveyard.Clear();
            }
            if (Deck.Count == 0) return;
            var n = Deck[0];
            Deck.RemoveAt(0);
            CurrentHand.Add(n);
        }
    }
    

    internal void UseAsSkill(CardPoolable card)
    {
        GainBlock(card.inst.def.Block);
        foreach (var si in card.inst.def.SelfStatus)
        {
            this.AddStatus(si.def, si.Stacks);
        }
    }

    internal void Setup(List<CardInstance> cards)
    {
        Deck = cards.Shuffled();
        DrawCards(5);
        Energy = 3;
        phui.SetCards(battle, CurrentHand);
    }

    internal void CardWasUsed(CardPoolable card)
    {
        if (CurrentHand.Contains(card.inst))
        {
            card.inst.RemainingCharges -= 1;
            CurrentHand.Remove(card.inst);

            if (card.inst.RemainingCharges <= 0)
            {
                if(card.inst.def.OnBreakReplacement != null)
                {
                    Graveyard.Insert(0, new CardInstance(card.inst.def.OnBreakReplacement));
                }
            }else{
                Graveyard.Insert(0, card.inst);
                phui.SetCards(battle, CurrentHand);
            }
        }
        else {
            throw new NotImplementedException();
        }
    }

    internal void DiscardHand()
    {
        Graveyard.AddRange(CurrentHand);
        CurrentHand.Clear();
        phui.SetCards(battle, CurrentHand);
    }
}