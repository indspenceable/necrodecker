﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Status Effect/Modifer")]
public class ConstantModifierStatusEffect : StatusDefinition
{
    public bool TickDownStartOfTurn;

    public override IEnumerator EndOfTurn(MookBase m, int stacks)
    {
        yield return null;
        if (!TickDownStartOfTurn)
            m.AddStatus(this, -1);
    }
    public override IEnumerator StartOfTurn(MookBase m, int stacks)
    {
        yield return null;
        if (TickDownStartOfTurn)
            m.AddStatus(this, -1);
    }
    public float _AttackMultiplier;
    public override float AttackMultiplier(int stacks)
    {
        return _AttackMultiplier;
    }
    public float _DamageTakenMultiplier;
    public override float DamageTakenMultiplier(int stacks)
    {
        return _DamageTakenMultiplier;
    }
    public int _PerHitGuard;
    public override int PerHitGuard(int stacks)
    {
        return _PerHitGuard;
    }
    public int _Strength;
    public override int Strength(int stacks)
    {
        return _Strength;
    }
}