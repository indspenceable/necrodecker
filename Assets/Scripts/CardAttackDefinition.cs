﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Attack")]
public class CardAttackDefinition : CardDefinition
{
    public List<StatusEffectInstance> AttackTargetStatus;

    public override CardType cardType => CardType.ATTACK;
    public override void UseAsAttack(PlayerInstance PI, MookBase mook)
    {
        mook.TakeAttack(PI, Power);
        foreach (var si in AttackTargetStatus)
        {
            mook.AddStatus(si.def, si.Stacks);
        }
    }
}