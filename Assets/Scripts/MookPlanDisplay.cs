﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MookPlanDisplay : MonoBehaviour
{
    public StatusEffectDisplay sed;
    public Mook mb;
    public StatusDefinition Attack;
    public StatusDefinition Defense;
    // Update is called once per frame
    void Update()
    {
        sed.Display(mb.currentPlan.Intents(Attack, Defense));
    }
}
