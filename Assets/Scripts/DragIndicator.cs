﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DragIndicator : AbstractPoolable
{
    public LineRenderer lr;
    public Vector3 Origin;
    public AutoReleasePool OnRelaseFX;

    public override void ExternalDeactivate()
    {
        gameObject.SetActive(false);
    }

    public override void OnActivatedByPool()
    {
        gameObject.SetActive(true);
        Origin = transform.position;
        lr.SetPosition(0, transform.position);
    }

    public void SetDest(Vector3 worldPosition)
    {
        lr.SetPosition(1, worldPosition);
    }

    public void ReleaseDrag(Vector3 worldPosition, bool useFX)
    {
        if (useFX)
            OnRelaseFX.RequestFX(worldPosition);
        ExternalDeactivate();
    }
}
