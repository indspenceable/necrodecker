﻿using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Skill")]
public class CardSkillDefinition : CardDefinition
{
    public override CardType cardType => CardType.SKILL;
    // Shouldn't happen!
    public override void UseAsAttack(PlayerInstance pI, MookBase mook) =>throw new System.NotImplementedException();
}
