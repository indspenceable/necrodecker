﻿using System.Collections.Generic;
using UnityEngine;


public abstract class CardDefinition : ReferrableScriptableObject
{
    public Sprite picture;
    [NaughtyAttributes.ResizableTextArea]
    public string text;

    public enum CardType
    {
        ATTACK,
        SKILL
    }
    public int cost = 1;
    public abstract CardType cardType { get; }
    public int Power;
    public int Block;
    public int DefaultCharges = 20;
    //When the card breaks, add this card to the graveyard instead.
    // If null, just remvoe the card.
    public CardDefinition OnBreakReplacement;
    public List<StatusEffectInstance> SelfStatus;

    public virtual void UseCard(PlayerInstance PlayerData, MookBase EnemyTarget)
    {
        PlayerData.GainBlock(Block);
        if (cardType == CardType.ATTACK)
        {
            EnemyTarget.TakeAttack(PlayerData, Power);
        }
    }

    public abstract void UseAsAttack(PlayerInstance pI, MookBase mook);
}
