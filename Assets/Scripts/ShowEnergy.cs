﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowEnergy : MonoBehaviour
{
    public TMPro.TextMeshProUGUI displayTextArea;
    public PlayerInstance PI;

    private void Update()
    {
        displayTextArea.text = PI.Energy.ToString();
    }
}
