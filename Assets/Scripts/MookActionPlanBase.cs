﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MookActionPlanBase : ScriptableObject, Mook.IMookActionPlan
{
    public abstract IEnumerator Enact(MookBase mook, CardBattle battle);
    public abstract List<StatusEffectInstance> Intents(StatusDefinition AttackDef, StatusDefinition GuardDef);
}
