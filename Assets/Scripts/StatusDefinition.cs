﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatusDefinition : ReferrableScriptableObject
{
    public Sprite sprite;
    public bool NegativesAllowed = false;

    public abstract IEnumerator StartOfTurn(MookBase mookBase, int stacks);
    public abstract IEnumerator EndOfTurn(MookBase mookBase, int stacks);
    public virtual float DamageTakenMultiplier(int stacks) => 1f;
    public virtual float AttackMultiplier(int stacks) => 1f;
    public virtual int PerHitGuard(int stacks) => 0;
    public virtual int Strength(int stacks) => 0;
}
