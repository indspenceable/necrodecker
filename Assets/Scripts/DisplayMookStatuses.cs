﻿using System.Collections.Generic;
using UnityEngine;

public class DisplayMookStatuses : MonoBehaviour
{
    public MookBase mook;
    public StatusEffectDisplay sed;
    private void Update()
    {
        sed.Display(mook.buffs);
    }
}
