﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName ="Object Pools/AutoRelease")]
public class AutoReleasePool : ObjectPoolBase<AutoReleasePoolable>
{
    public void RequestFX(Vector3 worldPosition) => Request(worldPosition);
}
