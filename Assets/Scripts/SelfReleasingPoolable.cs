﻿using System.Collections;
using System.Collections.Generic;

public abstract class AutoReleasePoolable : AbstractPoolable
{
    public override void OnActivatedByPool()
    {
        StartCoroutine(AutoRelease());
    }
    public IEnumerator AutoRelease() {
        while(true)
        {
            if (ShouldRelease()) ExternalDeactivate();
            yield return null;
        }
    }

    public abstract bool ShouldRelease();
}
