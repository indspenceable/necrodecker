﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class StatusEffectDisplay : MonoBehaviour
{
    public StatusEffectBadge PrototypeBadge;
    public List<StatusEffectBadge> badges;

    public void Display(List<StatusEffectInstance> statuses)
    {
        while (badges.Count < statuses.Count)
        {
            var newBadge = Instantiate(PrototypeBadge, transform);
            newBadge.gameObject.SetActive(true);
            badges.Add(newBadge);
        }
        while (badges.Count > statuses.Count)
        {
            Destroy(badges[0].gameObject);
            badges.RemoveAt(0);
        }
        for (int i = 0; i < badges.Count; i += 1) {
            StatusEffectInstance sei = statuses[i];
            badges[i].Show(sei);
        }
    }
}
