﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Mook : MookBase
{

    public interface IMookActionPlan
    {
        List<StatusEffectInstance> Intents(StatusDefinition AttackDef, StatusDefinition GuardDef);
        IEnumerator Enact(MookBase mook, CardBattle battle);
    }
    //private class AttackPlan : MookActionPlan
    //{
    //    public int Damage;
    //    public AttackPlan(int Damage)
    //    {
    //        this.Damage = Damage;
    //    }
    //    public override IEnumerator Enact(MookBase mook, CardBattle battle)
    //    {
    //        mook.anim.Play("MookAttack");
    //        yield return null;
    //        while (mook.anim.GetBool("AnimationActive")) yield return null;
    //        battle.PI.TakeAttack(battle.PI, Damage);
    //    }

    //    internal override string Format()
    //    {
    //        return $"Atk:{Damage}";
    //    }
    //}

    public MookPlannerBase AI;
    public IMookActionPlan currentPlan;
    public SpriteRenderer sr;

    public void SelectPlan()
    {
        this.currentPlan = AI.DeterminePlan();
    }

    public IEnumerator TakeTurn(CardBattle battle)
    {
        yield return StartOfTurn();
        if (!Dead())
            yield return this.currentPlan.Enact(this, battle);
        if (!Dead())
            yield return EndOfTurn();
    }

    internal void Setup(MookPlannerBase mpb)
    {
        sr.sprite = mpb.appearance;
        sr.transform.localScale = new Vector3(16f / mpb.appearance.rect.width, 16f / mpb.appearance.rect.width);
        this.HP = mpb.MaxHP;
        AI = mpb;
        buffs.Clear();
    }
}