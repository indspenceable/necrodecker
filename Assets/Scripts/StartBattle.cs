﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StartBattle : MonoBehaviour
{
    public BattleMode battleMode;
    public List<CardDefinition> defaultDeck;
    //public List<EncounterData> encounters;
    public Globals g;
    public void ExecStartBattle()
    {
        battleMode.ActivateWithCampaignAndEncounter(new CampaignData(defaultDeck.Select(c => new CardInstance(c)).ToList()),
            g.All<EncounterData>().Pick());
    }
}
