﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Status Effect/NoOp")]
public class NoOpStatus : StatusDefinition
{
    public override IEnumerator EndOfTurn(MookBase mookBase, int stacks) => null;
    public override IEnumerator StartOfTurn(MookBase mookBase, int stacks) => null;
}