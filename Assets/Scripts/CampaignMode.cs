﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CampaignData {
    public List<CardInstance> cards;

    public CampaignData(List<CardInstance> cards)
    {
        this.cards = cards;
    }
}

[CreateAssetMenu(menuName = "Modes/Campaign")]
public class CampaignMode : GameModeBase
{
    public TransitionEffect te;
    public CampaignData data;
    public void ActivateWithCampaign(CampaignData cd)
    {
        this.data = cd;
        Activate(te);
    }
}
