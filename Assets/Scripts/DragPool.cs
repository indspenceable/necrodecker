﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Object Pools/Drag Indicator")]
public class DragPool : ObjectPoolBase<DragIndicator>
{
    internal DragIndicator RequestWithOrigin(Vector3 worldPosition)
    {
        return Request(worldPosition);
    }
}
