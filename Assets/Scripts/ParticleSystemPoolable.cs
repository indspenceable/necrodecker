﻿using System;
using UnityEngine;

public class ParticleSystemPoolable : AutoReleasePoolable
{
    public ParticleSystem ps;
    public override void ExternalDeactivate()
    {
        gameObject.SetActive(false);
    }
    public override void OnActivatedByPool()
    {
        gameObject.SetActive(true);
        base.OnActivatedByPool();
    }

    public override bool ShouldRelease()
    {
        return !ps.IsAlive();
    }
}