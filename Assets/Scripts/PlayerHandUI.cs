﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandUI : MonoBehaviour
{
    public CardPool cardPool;
    public List<CardPoolable> currentPoolables = new List<CardPoolable>();
    public void SetCards(ICardDragHandler handler, List<CardInstance> cards) {
        foreach (var c in currentPoolables) c.ExternalDeactivate();
        currentPoolables.Clear();
        foreach (var c in cards) currentPoolables.Add(cardPool.RequestCard(handler, c, transform));
    }
}
