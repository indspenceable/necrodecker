﻿using UnityEngine;

[CreateAssetMenu(menuName = "Modes/Battle")]

public class BattleMode : GameModeBase
{
    public TransitionEffect te;
    public CampaignData campaignData;
    public EncounterData encounter;
    public void ActivateWithCampaignAndEncounter(CampaignData cd, EncounterData ed)
    {
        this.campaignData = cd;
        this.encounter = ed;
        Activate(te);
    }
}
