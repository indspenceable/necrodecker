﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Mook Actions/Generic")]
public class MookActionDefinition : MookActionPlanBase
{
    public int Damage;
    public int BlockAmt;
    public bool Animate = true;
    public List<StatusEffectInstance> AppliedStatuses;

    public override IEnumerator Enact(MookBase mook, CardBattle battle)
    {
        if (Animate)
        {
            mook.anim.Play("MookAttack");
            yield return null;
            while (mook.anim.GetBool("AnimationActive")) yield return null;
        }
        mook.CurrentBlock += BlockAmt;
        battle.PI.TakeAttack(battle.PI, Damage);
        foreach(var si in AppliedStatuses)
        {
            battle.PI.AddStatus(si.def, si.Stacks);
        }
    }

    public override List<StatusEffectInstance> Intents(StatusDefinition AttackDef, StatusDefinition GuardDef)
    {
        var rtn = new List<StatusEffectInstance>();
        if (Damage > 0) rtn.Add(new StatusEffectInstance(AttackDef, Damage));
        if (BlockAmt > 0) rtn.Add(new StatusEffectInstance(GuardDef, BlockAmt));
        rtn.AddRange(AppliedStatuses);
        return rtn;
    }
}
