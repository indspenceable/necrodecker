﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardBattle : GameModeInstance, ICardDragHandler
{
    public Camera cam;
    public BattleMode CardBattleMode;
    public StandardGameMode PostBattleMode;
    public PlayerHandUI phui;

    public PlayerInstance PI;
    // TODO the mooks should be dynamically placed at the start of the battle.
    public List<Mook> Mooks;

    public interface IBattleEventHandler
    {
        bool DragToMook(CardBattle battle, CardPoolable card, MookBase mook);
        bool DragToPlayer(CardBattle battle, CardPoolable card);
    }
    public class NoOpEventHandler : IBattleEventHandler
    {
        public bool DragToMook(CardBattle battle, CardPoolable card, MookBase mook) => false;
        public bool DragToPlayer(CardBattle battle, CardPoolable card) => false;
    }
    public class PlayerTurnEventHandler : IBattleEventHandler
    {
        public bool DragToMook(CardBattle battle, CardPoolable card, MookBase mook)
        {
            if (card.inst.def.cardType == CardDefinition.CardType.ATTACK)
            {
                if (card.inst.def.cost <= battle.PI.Energy)
                {
                    battle.PI.Energy -= card.inst.def.cost;
                    battle.PI.UseAsSkill(card);
                    card.inst.def.UseAsAttack(battle.PI, mook);
                    return true;
                }
            }
            return false;
        }

        public bool DragToPlayer(CardBattle battle, CardPoolable card)
        {
            if (card.inst.def.cardType == CardDefinition.CardType.SKILL)
            {
                if (card.inst.def.cost <= battle.PI.Energy)
                {
                    battle.PI.Energy -= card.inst.def.cost;
                    battle.PI.UseAsSkill(card);
                    return true;
                }
            }
            return false;
        }
    }
    public IBattleEventHandler currentEvent;

    public override void PreActivate(object DeserializedData)
    {
        if (DeserializedData != null)
        {
            // LOADING
        }
        else
        {
            // STARTING A NEW BATTLE
            PI.Setup(CardBattleMode.campaignData.cards);
            currentEvent = new PlayerTurnEventHandler();
            for (int i = Mooks.Count-1; i >= 0; i -= 1)
            {
                Mook m = Mooks[i];
                if (i < CardBattleMode.encounter.Monsters.Count)
                {
                    m.Setup(CardBattleMode.encounter.Monsters[i]);
                } else
                {
                    RemoveMookFromList(m);
                }
            }
            Mooks.ForEach(m => m.SelectPlan());
        }
    }

    public bool ExecuteDrag(CardPoolable card, Vector3 ped)
    {
        foreach (var mook in Mooks)
        {
            if (mook.bounds.OverlapPoint(ped))
            {
                if (currentEvent.DragToMook(this, card, mook))
                {
                    PI.CardWasUsed(card);
                    CheckForWinLoss();
                    return true;
                }
            }
        }

        if (PI.bounds.OverlapPoint(ped) && currentEvent.DragToPlayer(this, card))
        {
            PI.CardWasUsed(card);
            CheckForWinLoss();
            return true;
        }
        return false;
    }

    public void EndTurn()
    {
        PI.DiscardHand();
        PI.DrawCards(5);
        PI.Energy = 3;
        phui.SetCards(this, PI.CurrentHand);
        currentEvent = new NoOpEventHandler();
        StartCoroutine(TakeMonsterTurn());
    }

    private IEnumerator TakeMonsterTurn()
    {
        PI.EndOfTurn();
        foreach(Mook m in Mooks.ToList())
        {
            yield return m.TakeTurn(this);
            CheckForWinLoss();
        }
        yield return null;
        Mooks.ForEach(m => m.SelectPlan());
        yield return PI.StartOfTurn();
        currentEvent = new PlayerTurnEventHandler();
    }

    public void CheckForWinLoss()
    {
        List<Mook> deadMooks = Mooks.Where(m => m.Dead()).ToList();
        foreach (var m in deadMooks)
        {
            RemoveMookFromList(m);
        }

        if (Mooks.Count == 0) PostBattleMode.ActivateWithoutData(this.CardBattleMode.te);
        if (PI.Dead()) PostBattleMode.ActivateWithoutData(this.CardBattleMode.te);
    }

    private void RemoveMookFromList(Mook m)
    {
        m.gameObject.SetActive(false);
        Mooks.Remove(m);
    }
}
