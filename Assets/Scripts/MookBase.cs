﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MookBase : MonoBehaviour
{
    public Animator anim;
    public BoxCollider2D bounds;

    public int HP = 10;
    public int CurrentBlock;
    public List<StatusEffectInstance> buffs = new List<StatusEffectInstance>();

    public IEnumerator StartOfTurn()
    {
        CurrentBlock = 0;
        foreach (var si in buffs)
        {
            yield return si.def.StartOfTurn(this, si.Stacks);
        }
    }

    internal void AddStatus(StatusDefinition status, int delta)
    {
        var hits = buffs.Where(s => s.def == status);
        if (hits.Any())
        {
            var hit = hits.First();
            hit.Stacks += delta;
        } else
        {
            if (delta > 0 || status.NegativesAllowed)
            buffs.Add(new StatusEffectInstance(status, delta));
        }
    }

    public IEnumerator EndOfTurn()
    {
        foreach (var si in buffs)
        {
            yield return si.def.EndOfTurn(this, si.Stacks);
        }
    }

    internal void TakeAttack(MookBase Attacker, int power)
    {
        if (Attacker != null)
        {
            power = Mathf.Max(power + Attacker.AttackModifier(), 0);
            power = (int)(power * Attacker.AttackMultiplier());
        }

        power = Mathf.Max(power - CurrentGuard(), 0);
        power = (int)(power * DamageTakenMultiplier());

        if (CurrentBlock >= power)
        {
            CurrentBlock -= power;
            return;
        }
        power -= CurrentBlock;
        CurrentBlock = 0;
        HP -= power;
    }

    private float DamageTakenMultiplier()
    {
        return buffs.Aggregate(1f, (c, n) => c * n.def.DamageTakenMultiplier(n.Stacks));
    }

    private int CurrentGuard()
    {
        return buffs.Select(buff => buff.def.PerHitGuard(buff.Stacks)).Sum();
    }

    private float AttackMultiplier()
    {
        return buffs.Aggregate(1f, (c, n) => c * n.def.AttackMultiplier(n.Stacks));
    }

    private int AttackModifier()
    {
        return buffs.Select(buff => buff.def.Strength(buff.Stacks)).Sum();
    }

    internal bool Dead()
    {
        return HP <= 0;
    }
}
