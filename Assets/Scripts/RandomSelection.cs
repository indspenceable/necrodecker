﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Mook Planner/Random")]
public class RandomSelection : MookPlannerBase
{
    public List<MookActionPlanBase> PossibleActions;
    public override Mook.IMookActionPlan DeterminePlan()
    {
        return PossibleActions.Pick();
    }
}