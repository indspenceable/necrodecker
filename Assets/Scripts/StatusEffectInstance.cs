﻿using System;
using UnityEngine;

[System.Serializable]
public class StatusEffectInstance
{
    public StatusDefinition def;
    public Sprite sprite => def.sprite;
    public int Stacks;

    public StatusEffectInstance(StatusDefinition def, int stacks)
    {
        this.def = def;
        Stacks = stacks;
    }
}