﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class StatusEffectBadge : MonoBehaviour
{
    public Image im;
    public GameObject countArea;
    public TMPro.TextMeshProUGUI textArea;
    public bool HideOnes;
    internal void Show(StatusEffectInstance sei)
    {
        im.sprite = sei.sprite;
        if (sei.Stacks == 1)
        {
            countArea.SetActive(false);
        }
        else
        {
            textArea.text = sei.Stacks.ToString();
            countArea.SetActive(true);
        }
    }
}