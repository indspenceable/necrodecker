﻿using System.Collections.Generic;
using UnityEngine;

public class BlockDisplay : MonoBehaviour
{
    public TMPro.TextMeshProUGUI blockDisplay;
    public MookBase mook;

    // Update is called once per frame
    void Update()
    {
        blockDisplay.text = mook.CurrentBlock == 0 ? "" : $"{mook.CurrentBlock} blk";
    }
}
