﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartupInitialGameMode : MonoBehaviour
{
    public StandardGameMode mode;
    public TransitionEffect tEffect;
    // Start is called before the first frame update
    public IEnumerator Start()
    {
        yield return new WaitForSeconds(0f);
        mode.ActivateWithoutData(tEffect);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
