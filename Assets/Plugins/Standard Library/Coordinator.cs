﻿using UnityEngine;

public class Coordinator : MonoBehaviour
{
    public MusicManager mm;
    public Globals g;
    public Material TransitionMaterial;

    private void Start()
    {
        mm.OnGameStart(this);
    }
}