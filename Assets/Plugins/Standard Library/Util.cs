﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;


public static class Util
{
    public static Vector2 CenterOfTile(this Vector2Int v2i)
    {
        return v2i + new Vector2(0.5f, 0.5f);
    }

    public static bool Contains(this Vector2 v2, Vector2 o)
    {
        return (o.x >= 0 && o.x < v2.x && o.y >= 0 && o.y < v2.y);
    }
    public static bool Contains(this Vector2Int v2, Vector2 o)
    {
        return (o.x >= 0 && o.x < v2.x && o.y >= 0 && o.y < v2.y);
    }

    // in Vector2Int
    public static int ManhattanDistance(this Vector2Int a, Vector2Int b)
    {
        checked
        {
            return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
        }
    }
    public static int Manhattan(this Vector2Int v2i)
    {
        return v2i.ManhattanDistance(Vector2Int.zero);
    }
    public static float Distance(this Vector2Int a, Vector2Int b)
    {
        checked
        {
            return Vector2.Distance(a, b);
        }
    }
    public static bool Adjacent(this Vector2Int a, Vector2Int b)
    {
        checked
        {
            return ManhattanDistance(a, b) == 1;
        }
    }
    // in Vector3Int
    public static int ManhattanDistance(this Vector3Int a, Vector3Int b)
    {
        checked
        {
            return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y) + Mathf.Abs(a.z - b.z);
        }
    }

    /// <summary>
    /// Shuffles the element order of the specified list.
    /// </summary>
    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    public static List<T> Shuffled<T>(this IEnumerable<T> ts)
    {
        var dup = new List<T>(ts);
        dup.Shuffle();
        return dup;
    }

    public static T Pick<T>(this IEnumerable<T> ts)
    {
        return ts.Shuffled()[0];
    }

    public static T WeightedPick<T>(this IEnumerable<T> ts, System.Func<T, int> weight)
    {
        var pairs = ts.Select(o => new KeyValuePair<T, int>(o, weight(o))).Where(kvp => kvp.Value > 0).ToList();
        if (!pairs.Any()) Debug.LogError("#WeightedPick on a collection with no weighted elements");
        float pct = Random.Range(0f, 1f);
        float total = pairs.Select(p => p.Value).Sum();
        int index = 0;
        int SumValue = 0;


        while (true)
        {
            SumValue += pairs[index].Value;
            if (SumValue / total > pct) return pairs[index].Key;
            index += 1;
        }
        //return pairs[pairs.Count-1].Key;
    }

    public static T AggregateOrDefault<T>(this IEnumerable<T> ts, System.Func<T, T, T> ag, T DefaultValue)
    {
        if (ts.Count() > 0)
            return ts.Aggregate(ag);
        return DefaultValue;
    }

    public static IEnumerator MoveTransformToPosition(Transform targetUnit, Vector3 sp, Vector3 ep, AnimationCurve curve, float duration)
    {
        float dt = 0f;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.deltaTime;
            targetUnit.transform.position = Vector3.Lerp(sp, ep, curve.Evaluate(dt / duration));
        }
    }

    public static float Rounded(float v, int ToNearest)
    {
        return ((int)(v * ToNearest)) / (float)ToNearest;
    }
    public static Vector3 Rounded(this Vector3 vec)
    {
        return new Vector3(Rounded(vec.x, 16), Rounded(vec.y, 16));
    }
}
