﻿using System;
using UnityEngine;

public abstract class AbstractPoolable : MonoBehaviour
{
    public abstract void OnActivatedByPool();
    public abstract void ExternalDeactivate();

}
