﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

public struct MetaProgressionData
{

}

[CreateAssetMenu]
public class Globals : ScriptableObject
{
    public string VersionNumber = "0.0.1";
    public List<ReferrableScriptableObject> SOs = new List<ReferrableScriptableObject>();

    private class Vector2SerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            var v2i = (Vector2Int)obj;
            info.AddValue("x", v2i.x);
            info.AddValue("y", v2i.y);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            Vector2Int v2i = new Vector2Int((int)info.GetValue("x", typeof(int)), (int)info.GetValue("y", typeof(int)));
            return v2i;
        }
    }

    private class ScriptableObjectSerializationSurrogate : ISerializationSurrogate
    {
        public Globals globals;

        public ScriptableObjectSerializationSurrogate(Globals globals)
        {
            this.globals = globals;
        }

        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            var SO = (ReferrableScriptableObject)obj;
            info.AddValue("identifier", SO._HiddenIdentifier);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            string identifier = info.GetValue("identifier", typeof(string)) as string;
            ReferrableScriptableObject rtn = globals.SOs.Find(talent => talent._HiddenIdentifier == identifier);
            if (rtn == null || identifier == "" || identifier == null)
            {
                Debug.LogError("Was unable to deserialize scriptable object with identifier: " + identifier);
            }
            return rtn;
        }
    }


    void AddSurrogates(SurrogateSelector selector, List<Type> forTypes, ISerializationSurrogate surrogate)
    {
        foreach (var type in forTypes)
        {
            selector.AddSurrogate(type, new StreamingContext(StreamingContextStates.All), surrogate);
        }
    }

    public BinaryFormatter BuildFormatter()
    {
        var rtn = new BinaryFormatter();
        SurrogateSelector surrogateSelector = new SurrogateSelector();
        surrogateSelector.AddSurrogate(typeof(Vector2Int),
            new StreamingContext(StreamingContextStates.All),
            new Vector2SerializationSurrogate());
        AddSurrogates(surrogateSelector,
            SOs.Select(t => t.GetType())
                //.Append(typeof(Talent))
                //.Append(typeof(AIStrategyBase)).Distinct()
                .ToList(),
            new ScriptableObjectSerializationSurrogate(this));
        rtn.SurrogateSelector = surrogateSelector;
        return rtn;
    }

    [System.Serializable]
    public class SerializedData
    {
        public string Version;
        public GameModeBase gm;
        public object o;

        public SerializedData(string Version, GameModeBase gm, object o)
        {
            this.Version = Version;
            this.gm = gm;
            this.o = o;
        }
    }

    private string QuicksaveFilePath()
    {
        return Application.persistentDataPath + "/quicksave";
    }

    public bool QuicksaveExists()
    {
        return File.Exists(QuicksaveFilePath());
    }

    internal void WipeQuicksaveData()
    {
        File.Delete(QuicksaveFilePath());
    }

    private Dictionary<Type, object> cachedLists;
    public IEnumerable<T> All<T>() where T : ReferrableScriptableObject
    {
        if (cachedLists == null) cachedLists = new Dictionary<Type, object>();
        var key = typeof(T);
        if (!cachedLists.ContainsKey(key))
        {
            cachedLists[key] = this.SOs.Where(so => so is T).Cast<T>().ToList();
        }
        return (List<T>)cachedLists[key];
    }

    public void Quicksave(GameModeBase gm, object o)
    {
        BinaryFormatter binFormatter = BuildFormatter();
        FileStream file = File.Create(QuicksaveFilePath()); //you can call it anything you want
        binFormatter.Serialize(file, new SerializedData(VersionNumber, gm, o));
        file.Close();
    }

    public void LoadQuicksave(TransitionEffect te)
    {
        BinaryFormatter binFormatter = BuildFormatter();
        FileStream file = File.Open(QuicksaveFilePath(), FileMode.Open);
        System.Action Todo = null;
        try
        {
            var data = (SerializedData)binFormatter.Deserialize(file);
            if (data.Version == VersionNumber)
            {
                Todo = () => data.gm.ActivateFromQuicksave(te, data.o);
            }
        }
        catch (Exception)
        {

        }
        finally
        {
            file.Close();
        }

        if (Todo != null)
        {
            Todo();
        }
        else
        {
            var ap = All<AudioPool>().First();
            ap.PlaySimple(ap.sfx.Thud);
        }
    }

    public string MetaSaveFilePath()
    {
        return Application.persistentDataPath + "/persistent_data";
    }

    public void SaveMetaProgression(MetaProgressionData prog)
    {
        BinaryFormatter binFormatter = BuildFormatter();
        FileStream file = File.Create(MetaSaveFilePath()); //you can call it anything you want
        binFormatter.Serialize(file, prog);
        file.Close();
    }

    public MetaProgressionData LoadMetaProgression()
    {
        MetaProgressionData rtn;
        if (File.Exists(MetaSaveFilePath()))
        {
            BinaryFormatter binFormatter = BuildFormatter();
            FileStream file = File.Open(MetaSaveFilePath(), FileMode.Open);
            rtn = (MetaProgressionData)binFormatter.Deserialize(file);
            file.Close();
        }
        else
        {
            rtn = new MetaProgressionData();
        }
        return rtn;
    }
    
#if UNITY_EDITOR
    [NaughtyAttributes.Button]
    public void Editor_Only_RefreshAll()
    {
        SOs = AssembleAllInstancesInProject<ReferrableScriptableObject>().ToList();
        SOs.ForEach(so => UnityEditor.EditorUtility.SetDirty(so));
        UnityEditor.EditorUtility.SetDirty(this);
        this.cachedLists = null;
    }

    public static T[] AssembleAllInstancesInProject<T>() where T : ScriptableObject
    {
        string[] guids = UnityEditor.AssetDatabase.FindAssets("t:" + typeof(T).Name);  //FindAssets uses tags check documentation for more info
        T[] a = new T[guids.Length];
        for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
        {
            string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(path);
        }

        return a;

    }

    public static Sprite[] GetAllSprites()
    {
        string[] guids = UnityEditor.AssetDatabase.FindAssets("t:Sprite");  //FindAssets uses tags check documentation for more info
        Sprite[] a = new Sprite[guids.Length];
        for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
        {
            string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = UnityEditor.AssetDatabase.LoadAssetAtPath<Sprite>(path);
        }

        return a;

    }
#endif

    public T Duplicate<T>(T obj)
    {
        Stream stream = new MemoryStream();
        BinaryFormatter binFormatter = BuildFormatter();
        binFormatter.Serialize(stream, obj);
        stream.Seek(0, SeekOrigin.Begin);
        return (T)binFormatter.Deserialize(stream);
    }

    //public T FindById<T>(string Identifier) where T : ReferrableScriptableObject
    //{
    //    var matches = All<T>().Where(o => o.Identifier == Identifier);
    //    if (!matches.Any())
    //        Debug.LogError($"Unable to find: {typeof(T)} with ID: {Identifier}");
    //    return matches.First();
    //}


    [NaughtyAttributes.Button]
    public void ValidateDataPlease()
    {
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }
}
