﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour
{
    public Vector3 SpawnLocation;
    public Globals g;
    private void Start()
    {
        foreach (AbstractObjectPool p in g.All<AbstractObjectPool>())
        {
            p.Setup(this);
            StartCoroutine(BuildUpPools(p));
            if (p.RequiresUpdate()) StartCoroutine(RunUpdates(p));
        }
    }

    IEnumerator RunUpdates(AbstractObjectPool pool)
    {
        while(true)
        {
            pool.OnUpdate();
            yield return null;
        }
    }

    IEnumerator BuildUpPools(AbstractObjectPool pool)
    {
        //WaitForSeconds wfs = new WaitForSeconds(0.2f);
        //List<Poolable> dummyObjects = new List<Poolable>();
        yield return new WaitForSeconds(0.25f);

        while (pool.CurrentCapacity < pool.DesiredCapacity)
        {
            for (int i = 0; i < pool.IncreasedCapacityPerFrame; i += 1)
            {
                var poolable = pool.RequestNew(SpawnLocation);
                poolable.ExternalDeactivate();
            }
            yield return null;
        }
    }
}
