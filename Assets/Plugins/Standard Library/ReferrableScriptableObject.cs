﻿using System;
using System.Linq;
using UnityEngine;

public class ReferrableScriptableObject : ScriptableObject
{
    [NaughtyAttributes.ReadOnly]
    public string _HiddenIdentifier;

    private void OnValidate()
    {
        if (_HiddenIdentifier == null ||
            _HiddenIdentifier == "" ||
            Resources.FindObjectsOfTypeAll<ReferrableScriptableObject>().Any(t => t._HiddenIdentifier == _HiddenIdentifier && t != this))
        {
            _HiddenIdentifier = Guid.NewGuid().ToString();
        }
    }
}
