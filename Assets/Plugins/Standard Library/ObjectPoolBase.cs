﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractObjectPool : ReferrableScriptableObject
{
    public int CurrentCapacity
    {
        get;
        protected set;
    }
    public int DesiredCapacity = 1;
    public int IncreasedCapacityPerFrame = 1;

    public abstract AbstractPoolable RequestNew(Vector3 position);

    public abstract void Setup(ObjectPoolManager objectPoolManager);
    public virtual bool RequiresUpdate() => true;
    public virtual void OnUpdate() { }
}

public class ObjectPoolBase<T> : AbstractObjectPool where T : AbstractPoolable
{
    [NaughtyAttributes.ReadOnly]
    private ObjectPoolManager opm;
    public T prefab;

    //public HashSet<Poolable> ActiveInstances;
    private Queue<T> InactiveInstances;

    public override void Setup(ObjectPoolManager objectPoolManager)
    {
        this.opm = objectPoolManager;
        InactiveInstances = new Queue<T>();
        CurrentCapacity = 0;
    }
    public override AbstractPoolable RequestNew(Vector3 position)
    {
        if (prefab == null) Debug.LogError(this);
        var rtn = Instantiate(prefab, position, Quaternion.identity, opm.transform);
        //rtn.gameObject.SetActive(false);
        rtn.OnActivatedByPool();
        CurrentCapacity += 1;
        return rtn;
    }
    protected T Request(Vector3 position)
    {
        T instance = null;
        while (instance == null && InactiveInstances != null && InactiveInstances.Count > 0)
        {
            instance = InactiveInstances.Dequeue();
        }
        if (instance == null)
        {
            return RequestNew(position) as T;
        }
        instance.transform.position = position;
        instance.OnActivatedByPool();
        return instance;
    }
    public void Released(T g)
    {
        InactiveInstances.Enqueue(g);
    }
}
