﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu()]
public class MusicManager : ScriptableObject
{
    private Coordinator coordinator;
    private AudioSource audioSource = null;
    [NaughtyAttributes.ReadOnly]
    public float CrossfadeCurrentVolumePercent;

    internal void OnGameStart(Coordinator coordinator)
    {
        // Whatever setup needs to happen here.
        this.coordinator = coordinator;
        CrossfadeCurrentVolumePercent = 1f;
        audioSource = new GameObject("Music").AddComponent<AudioSource>();
        audioSource.loop = false;
        coordinator.StartCoroutine(LateUpdateCo());
    }

    private IEnumerator LateUpdateCo()
    {
        while (true)
        {
            SetMusicVolume();
            yield return null;
        }
    }

    private void SetMusicVolume()
    {
        this.audioSource.volume = PlayerPrefs.GetFloat("MasterVolume", 0.7f) * PlayerPrefs.GetFloat("MusicVolume", 0.7f) * CrossfadeCurrentVolumePercent;
    }

    //public void FadeToNone()
    //{
    //    FadeTo(null);
    //}
    //public void FadeTo(AudioClip TargetMusic)
    //{
    //    coordinator.StartCoroutine(FadeOver(TargetMusic, 1f));
    //}
    //public void FadeToInstant(AudioClip TargetMusic)
    //{
    //    coordinator.StartCoroutine(FadeOver(TargetMusic, 0f));
    //}
    //public void Cut()
    //{
    //    coordinator.StartCoroutine(FadeOver(null, 0.2f));
    //}
    //public IEnumerator FadeOver(AudioClip TargetMusic, float durationOut, float durationIn = 0f)
    //{
    //    if (audioSource.clip == TargetMusic) yield break;

    //    float dt = 0f;

    //    if (audioSource.clip != null)
    //    {
    //        while (dt <= durationOut)
    //        {
    //            if (durationOut > 0f)
    //                yield return null;
    //            dt += Time.unscaledDeltaTime;
    //            CrossfadeCurrentVolumePercent = Mathf.Clamp01(1 - (dt / durationOut));
    //            SetMusicVolume();

    //        }
    //        audioSource.Stop();
    //    }
    //    audioSource.clip = TargetMusic;
    //    if (audioSource.clip != null)
    //    {
    //        audioSource.Play();
    //        dt = 0f;
    //        while (dt <= durationIn)
    //        {
    //            if (durationIn > 0f)
    //                yield return null;
    //            dt += Time.unscaledDeltaTime;
    //            CrossfadeCurrentVolumePercent = Mathf.Clamp01(dt / durationOut);
    //            SetMusicVolume();
    //        }
    //    }
    //    CrossfadeCurrentVolumePercent = 1f;
    //    SetMusicVolume();
    //}

    //public void SilenceInstant()
    //{
    //    CrossfadeCurrentVolumePercent = 0f;
    //}
}