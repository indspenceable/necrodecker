﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Modes/Standard")]
public class StandardGameMode : GameModeBase
{
    public void ActivateWithoutData(TransitionEffect tEffect)
    {
        this.Activate(tEffect);
    }
}
