﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPoolable : AbstractPoolable
{
    public AudioSource audioSource; 
    public AudioPool pool;
    public override void ExternalDeactivate()
    {
        pool.Released(this);
        gameObject.SetActive(false);
    }

    public override void OnActivatedByPool()
    {
        gameObject.SetActive(true);
    }

    public void Play(AudioClip clip, float volume, float pitch)
    {
        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.pitch = pitch;
        audioSource.loop = false;
        audioSource.Play();
        StartCoroutine(DeactivateWhenClipIsFinished());
    }

    public float pctComplete()
    {
        return audioSource.time / audioSource.clip.length;
    }

    IEnumerator DeactivateWhenClipIsFinished()
    {
        while(true)
        {
            yield return null;
            if (!audioSource.isPlaying) ExternalDeactivate();
        }
    }
}