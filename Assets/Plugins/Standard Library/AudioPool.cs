﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SFX
{
    public AudioClip Thud;
}

[CreateAssetMenu(menuName = "Object Pools/Audio")]
public class AudioPool : ObjectPoolBase<AudioPoolable>
{
    public float PlaySimplePct = 0.85f;
    private List<AudioClip> playedThisFrame = new List<AudioClip>();

    public SFX sfx;

    public AudioPoolable Play(AudioClip clip, float v, float pitch)
    {
        if (playedThisFrame.Contains(clip)) return null;
        if (clip == null) Debug.Log("AudioPool#Play with a null clip");
        var rtn = Request(Vector3.zero);
        rtn.Play(clip, Volume(v), pitch);
        playedThisFrame.Add(clip);
        return rtn;
    }
    public void PlaySimple(AudioClip clip)
    {
        Play(clip, PlaySimplePct, 1f);
    }
    public void PlayQuiet(AudioClip clip)
    {
        Play(clip, PlaySimplePct / 3f, 1f);
    }
    public override void OnUpdate()
    {
        playedThisFrame.Clear();
    }
    public override bool RequiresUpdate()
    {
        return true;
    }

    public float Volume(float v)
    {
        return v * PlayerPrefs.GetFloat("MasterVolume", 0.7f) * PlayerPrefs.GetFloat("SFXVolume", 1f);
    }
}