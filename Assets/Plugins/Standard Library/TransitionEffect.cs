﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TransitionEffect/Standard")]
public class TransitionEffect : ScriptableObject
{
    public Texture2D In;
    public float InDuration = 0.25f;
    public Texture2D Out;
    public float OutDuration = 0.25f;

    public virtual void PreDeactivate() { }
}
